<?php
	//Establece la variable de autor.
	$autor = "Artur";

	//Establece la variable de titulo, que se usara en el archiv header.php para mostrar la información del <head> de titulo.
	//Aqui se junta Actividad 1 - M14 con la variable autor.
	$title = "Actividad 1 - M14 ".$autor;

	// Se incluye el archivo header.php de la ruta includes, ahí está el codigo del <head>, esto nos permitira reusarlo para 
	// varias paginas.
	include "includes/header.php";

	// Implime codigo html dentro del <body>, el tag de body esta incluido en el archivo header.php en la ultima linea, por lo cual a partir de aqui es codigo que se ejecuta dentro del body html.
	// Se muestra una tarjeta con el hello world, contiene clases para usar estilos tanto propios como de Bootstrap.
	echo "<div class='card helloworld'>
		<img class='card-img-top helloworldimg' alt='img' src='img/helloworld.png'>
		<h5 class='card-title'>Hello World!</h5>
		<p class='card-text'> un hello world. </p>
		<a href='#' class='btn btn-danger'> pulsa aqui </a>
		</div>";

	//Aqui imprimos otro codigo html en el que mostraremos una tarjeta con mi nombre, una imagen y la fecha de ejecucion.
	echo "<div class='card otrainfo'>
		<img class='card-img-top iimg' alt='img' src='img/artur.png'>
		<h5 class='card-title'>".$autor."</h5>
		<p class='card-text'> ".date("Y-M-d-H:i")." </p>
		<a href='#' class='btn btn-danger'> pulsa aqui </a>
		</div>";

	//Se incluye el footer.php, el cual contiene entre otras cosas el final del </body>.
	include "includes/footer.php";
?>
