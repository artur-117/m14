document.addEventListener("DOMContentLoaded", function() {
    function generarTabla() {
	// Establece todas las variables a id's del html.
        var numero = document.getElementById("num").value;
        var resultado = document.getElementById("resultado");
	
 	// Si el numero es mayor a longitud 0, lo procesa.
        if (numero.length > 0) {
 	    // Manda un post al html con los datos del numero.
            $.post("controller/procesarnumeros.php", { num: numero })
                .done(function(response) {
			// Muestra la respuesta en tiempo real.
                	var res = eval(response);
			if(res[0]==0){
				resultado.innerHTML = res[1]+"<br>"+res[2]+"<br>";
				$("input").css("background-color", "lightcoral");
			}else{ 
				resultado.innerHTML = res[1]+"<br>"+res[2]+"<br>"+res[3]+"<br>"+res[4];
				$("input").css("background-color", "lightgreen");
			}
		});
        } else {
	    // Si no es mayor a 0 la longitud, no muestra nada.
            resultado.textContent = "Faltan 10 numeros.";
        }
    }

    // Detecta si se esta editando.
    document.getElementById("num").addEventListener("keyup", generarTabla);
});

