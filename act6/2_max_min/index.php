<?php
	$title="Tabla multiplicar";
	$extrascript="control.js";
	include "includes/header.php";
?>
	<!--Formulario de la tabla, puedes establecer la longitud y un numero a multiplicar.-->
	<form action="controller/obtener_tabla.php" method="post">
		<fieldset>
			<legend>Introduce 10 numeros, separados por comas (,)</legend>
			<label for="num">Numeros: </label>
                	<input type="text" id="num" name="num"><br>
		</fieldset>
	</form>

	<!-- Por aqui mostrara el resultado en tiempo real, usando jquery+php. -->
	<div id="resultado">
	
	</div>

<?php
	include "includes/footer.php";
?>
