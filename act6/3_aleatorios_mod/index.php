<?php
	$title="Tabla multiplicar";
	$extrascript="reiniciar.js";
	$extrascript2="control.js";
	include "includes/header.php";
?>

	<button id="bu"> Reiniciar aleatorios </button>
	<h2> Numeros Aleatorios </h2>
	<div id="aleatorios"></div>


	<!--Formulario de la tabla, puedes establecer la longitud y un numero a multiplicar.-->
	<form action="controller/obtener_tabla.php" method="post">
		<fieldset>
			<legend>Introduce los numeros.</legend>
			<label for="num">Numero original: </label>
                	<input type="text" id="num" name="num"><br>
			<label for="nummod">Numero a modificar: </label>
                	<input type="text" id="nummod" name="nummod"><br>
		</fieldset>
	</form>

	<h2> Numeros Modificados </h2>
	<!-- Por aqui mostrara el resultado en tiempo real, usando jquery+php. -->
	<div id="resultado">
	
	</div>

<?php
	include "includes/footer.php";
?>
