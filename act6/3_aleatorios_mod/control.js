document.addEventListener("DOMContentLoaded", function() {
    function modificarAleatorios() {
	// Establece todas las variables a id's del html.
        var numoriginal = document.getElementById("num").value;
        var nummodificar = document.getElementById("nummod").value;
        var resultado = document.getElementById("resultado");
        var aleatorios = document.getElementById("aleatorios");
	
 	// Si el numero es mayor a longitud 0, lo procesa.
        if (aleatorios.innerHTML != "") {
 	    // Manda un post al html con los datos del numero.
            $.post("controller/procesarnumeros.php", { original: numoriginal, modificar: nummodificar,num: aleatorios.innerHTML })
                .done(function(response) {
			// Muestra la respuesta en tiempo real.
			resultado.innerHTML = response;
		});
        } else {
	    // Si no es mayor a 0 la longitud, no muestra nada.
            aleatorios.innerHTML = "¡GENERA LOS ALEATORIOS ANTES!";
        }
    }

    // Detecta si se esta editando.
    document.getElementById("num").addEventListener("keyup", modificarAleatorios);
    document.getElementById("nummod").addEventListener("keyup", modificarAleatorios);
    document.getElementById("bu").addEventListener("click", modificarAleatorios);
});

