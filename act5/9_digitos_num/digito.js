document.addEventListener("DOMContentLoaded", function() {
    function generarDigito() {
 	// Establece las variables para las id's.
	var numero = document.getElementById("num").value;
        var resultado = document.getElementById("resultado");
	
	// Si su longitud es mayor a 0, lo procesa.
        if (numero.length > 0) {
	    // Manda un post al php con el numero.
            $.post("controller/digitos.php", { num: numero })
                .done(function(response) {
			// Muestra la respuesta.
                	resultado.innerHTML = response;
		});
        } else {
	    // Si no es mayor a 0, mostrara un 0.
            resultado.textContent = "0";
        }
    }

    // Detecta si se ha editado y ejecuta la funcion generarDigito.
    document.getElementById("num").addEventListener("keyup", generarDigito);
});

