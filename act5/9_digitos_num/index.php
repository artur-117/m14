<?php
	$title="Digitos";
	$extrascript="digito.js";
	include "includes/header.php";
?>
	<!--Formulario para dejar el numero.-->
	<form action="controller/digitos.php" method="post">
		<fieldset>
			<legend>Obtener digitos</legend>
			<label for="num">Numero: </label>
                	<input type="text" id="num" name="num"><br>
		</fieldset>
	</form>

	<!-- En este contenedor veremos el resultado dado por jquery+php-->
	<div id="resultado">
	
	</div>

<?php
	include "includes/footer.php";
?>
