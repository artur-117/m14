<?php
	$title="Tabla multiplicar";
	$extrascript="generartabla.js";
	include "includes/header.php";
?>
	<!--Formulario de la tabla, puedes establecer la longitud y un numero a multiplicar.-->
	<form action="controller/obtener_tabla.php" method="post">
		<fieldset>
			<legend>Tabla multiplicar</legend>
			<label for="tab">Tabla: </label>
                	<input type="text" id="tab" name="tab"><br>
			<label for="num">Numero: </label>
                	<input type="text" id="num" name="num"><br>
		</fieldset>
	</form>

	<!-- Por aqui mostrara la tabla en tiempo real, usando jquery+php. -->
	<div id="tabla">
	
	</div>

<?php
	include "includes/footer.php";
?>
