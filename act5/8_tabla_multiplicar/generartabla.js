document.addEventListener("DOMContentLoaded", function() {
    function generarTabla() {
	// Establece todas las variables a id's del html.
        var numero = document.getElementById("num").value;
        var tab = document.getElementById("tab").value;
        var tabla = document.getElementById("tabla");
	
 	// Si el numero es mayor a longitud 0, lo procesa.
        if (numero.length > 0) {
 	    // Manda un post al html con los datos de tabla y numero.
            $.post("controller/obtener_tabla.php", { tab: tab, num: numero })
                .done(function(response) {
			// Muestra la respuesta en tiempo real.
                	tabla.innerHTML = response;
		});
        } else {
	    // Si no es mayor a 0 la longitud, no muestra nada.
            tabla.textContent = "";
        }
    }

    // Detecta si se esta editando.
    document.getElementById("num").addEventListener("keyup", generarTabla);
    document.getElementById("tab").addEventListener("keyup", generarTabla);
});

