<?php
	session_start();
	$title="Combinacion segura.";
	$extrascript="comprobar.js";
	include "includes/header.php";
?>

	<!--Formulario para procesar la combinacion.-->
	<form action="controller/comprobar.php" method="post">
		<fieldset>
			<legend>Combinacion Caja Fuerte.</legend>
			<label for="num">Combinacion: </label>
                	<input type="text" id="num" name="num"><br>
                	<input type="submit">
		</fieldset>
	</form>


	<!--Formulario para resetear las oportunidades.-->
	<form action="controller/reiniciar.php" method="post">
		<fieldset>
                	<input type="submit" value="Reiniciar.">
		</fieldset>
	</form>




	<!--Resultados, de oportunidades y texto.-->
	<div id="resultado">
		<?php
			echo "Oportunidad: ".$_SESSION["oportunidad"]."<br>";
			if($_SESSION["oportunidad"]==4){
				echo "Intenta alguna combinación...";
			}else if($_SESSION["oportunidad"]<3){
				echo "Lo siento, esta no es la combinacion...";
			}
		?>
	</div>
<?php
	include "includes/footer.php";
?>
