<?php
	// Inicia la sesion, detecta si hay alguna combinacion.
	session_start();
	if(!empty($_POST["num"])){
		// Guarda combinacion en $n
		$n = $_POST["num"];
		// Si ya tiene 0 oportunidades, vuelve sin procesar lo demás.
		if($_SESSION["oportunidad"]==0){
				header("location: ../index.php");
		}

		// Mientras las oportunidades sean mayores a 0, procesa este codigo.
		while($_SESSION["oportunidad"]>0){
			// Si la combinacion es 1234, el usuario ha acertado, muestra un texto junto a una
			// imagen y le pone las oportunidades a 0, ya que no necesita más.
			if($n==1234){
				echo "¡¡¡¡GANASTE!!!!<br>";
				echo "<img width=100 src=../img/dinero.jpg>";
				$_SESSION["oportunidad"] = 0;
			// Si no, le resta una oportunidad, la guarda y vuelve al index.
			}else{
				$b = $_SESSION["oportunidad"]-1;
        			$_SESSION["oportunidad"] = $b;
				header("location: ../index.php");
				break;
			}
		}
		
		}else{
		echo "Esta vacio.";
	}
?>
