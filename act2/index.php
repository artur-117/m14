<?php

	// Función que devuelve un color random
	function randomcolor() {
    		return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
	}
	// Define autor
	$autor = "Artur Kravchuk Tarasyuk";
	// Define el titulo como practica 2 y autor
	$title = "Practica 2 ".$autor;

	// Estilo css extra
	$extra = "<link rel='stylesheet' href='css/style.css'>";
	// Incluye el header.php
	include "includes/header.php";

	//Define color con la función randomcolor que devuelve un color aleatorio.
	$color = randomcolor();
	//Muestra el body con estilo de color aleatorio y el titulo en h1.
	echo "<body style='background-color:".$color.";'><h1 class='b'>".$title."</h1>";


	// Define la array con todos los nombres de variable para la tabla.
	$nv = array(
		"SERVER PORT",
		"DOCUMENT ROOT",
		"REMOTE ADDR",
		"HTTP_X_FORWARDED_FOR",
		"SERVER ADDR",
		"SERVER NAME",
		"SERVER_SIGNATURE",
		"SCRIPT_NAME",
		"COLOR"
	);

	// Define la array con todas las variables para la tabla.
	$v = array(
		$_SERVER['SERVER_PORT'],
		$_SERVER['DOCUMENT_ROOT'],
		$_SERVER['REMOTE_ADDR'],
		$_SERVER['HTTP_X_FORWARDED_FOR'],
		$_SERVER['SERVER_ADDR'],
		$_SERVER['SERVER_NAME'],
		$_SERVER['SERVER_SIGNATURE'],
		$_SERVER['SCRIPT_NAME'],
		$color
	);

	// Define la array con todas las descripciónes para la tabla.
	$d = array(
		"Puerto de la maquina que se esta usando para la comunicación con el servidor.",
		"Dirección raiz de los documentos del servidor donde se ejecutan los script's actuales. Según estan definidos en la configuración 		  del servidor.",
		"Dirección IP desde donde vemos la pagina actual de usuario [Se muestra la ip del Proxy]",
		"Dirección IP del usuario",
		"Dirección IP del servidor desde donde se ejecuta el script.",
		"Nombre del host donde se ejecuta el script.",
		"Cadena que contiene la versión del servidor y nombre virtual del host que se añade a las paginas que se generan desde el 
		servidor si esta enable.",
		"Nombre del script ejecutado junto a su ruta.",
		"Color de fondo de la pagina."
	);

	// Incluye la tabla.php
	include "tabla.php";
	
	// Muestra la imagen.
	echo "<img src='img/php.png' alt='php'>";

	// Muestra la fecha actual, la de creación y el autor.
	echo "Fecha actual: ".date('d-m-Y h:i');
	echo "<br>Fecha creación: ".date('d-m-Y h:i', filemtime("index.php"));
	echo "<br>Autor: <span class='autor'>".$autor."</span>";
	
	// Incluye footer.php
	include "includes/footer.php";
?>
