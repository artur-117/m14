<?php
	// Titulo de la pagina.
	$title = "Nota media.";
	include "includes/header.php";

?>
	<!--Formulario para la nota media..-->
	<form action="controller/calcular.php" method="post">
		<fieldset>
			<legend>Notas</legend>
			<label for="nota1">Nota 1: </label>
			<input type="text" id="nota1" name="nota1"><br>
			<label for="nota2">Nota 2: </label>
			<input type="text" id="nota2" name="nota2"><br>
			<label for="nota3">Nota 3: </label>
			<input type="text" id="nota3" name="nota3"><br>

		</fieldset>
		<!--Boton submit para enviar las horas al php por metodo post.-->
		<input type="submit" name="calcular" id="calcular" value="calcular">
	</form>

<?php
	include "includes/footer.php";
?>
