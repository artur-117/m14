<?php
	// Titulo de la pagina.
	$title = "Ecuaciones";
	include "includes/header.php";

?>
	<!--Formulario para calcular la ecuación.-->
	<form action="controller/calcular.php" method="post">
		<fieldset>
			<legend>Ecuaciones</legend>
			<label for="ecuacion">Ecuacion: </label>
			<input type="text" id="ecuacion" name="ecuacion"><br>
		</fieldset>
		<!--Boton submit para enviar las horas al php por metodo post.-->
		<input type="submit" name="calcular" id="calcular" value="calcular">
	</form>
<!-- Ejemplos de input valido -->
<br>
	<p>Ejemplos de input: </p>
	<p>[a*x+b=0]</p>
	<p>5*x+10=0</p>
	<p>2*x+4=0</p>
	<p>6*x+30=0</p>


<?php
	include "includes/footer.php";
?>
