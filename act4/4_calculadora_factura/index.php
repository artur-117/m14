<?php
	// Titulo de la pagina.
	$title = "Calculadora factura.";
	include "includes/header.php";

?>
	<!--Formulario para calcular la factura.-->
	<form action="controller/calcular.php" method="post">
		<fieldset>
			<legend>Facturas totales</legend>
			<label for="hora">Horas trabajadas: </label>
			<input type="text" id="hora" name="hora"><br>
		</fieldset>
		<!--Boton submit para enviar las horas al php por metodo post.-->
		<input type="submit" name="calcular" id="calcular" value="calcular">
	</form>
<?php
	include "includes/footer.php";
?>
