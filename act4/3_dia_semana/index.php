<?php
	// Titulo de la pagina.
	$title = "Dias.";
	include "includes/header.php";

?>
	<!--Formulario para obtener el dia..-->
	<form action="controller/obtenerdia.php" method="post">
		<fieldset>
			<legend>Obtener dia dependiendo del numero dado.</legend>
			<label for="dia">Dia:</label>
			<input type="text" id="dia" name="dia"><br>
		</fieldset>
		<!--Boton submit para enviar el dia al php por metodo post.-->
		<input type="submit" name="obtenerdia" id="obtenerdia" value="Obtener dia">
	</form>

<?php
	include "includes/footer.php";
?>
