<?php
	// Comprueba si se ha obtenido el saludo, si no da error.
	if(!empty($_POST["obtenersaludo"])){
		$hora=$_POST["hora"];

		// Comprueba la hora, depende la hora da un buen dia, un buenas tardes o buenas noches.
		if(5<$hora&&$hora<13){
			echo "<h1 style='font-size:5vh'>¡Buen día!</h1>";
		}
		else if(12<$hora&&$hora<21){
			echo "<h1 style='font-size:5vh'>¡Buenas Tardes!</h1>";
		}	
		else if(20<$hora&&$hora<24||$hora<6){
			echo "<h1 style='font-size:5vh'>¡Buenas Noches!</h1>";
		}else{
			echo "Fuera de limites, debes poner una hora del 0 al 23.<br>";
		}
	}else{
		echo "error";
	}
?>
