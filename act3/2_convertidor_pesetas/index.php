<?php
	// Titulo de la pagina.
	$title = "Convertidor Euro-Peseta";
	include "includes/header.php";

?>
	<!--Formulario euros-pesetas-->
	<form action="controller/calcular.php" method="post">
		<fieldset>
			<legend>Convertidor Euros-Pesetas</legend>
			<label for="eur">Euros:</label>
			<input type="text" id="eur" name="eur"><br>
		</fieldset>
		<!--Submit para enviar los datos al php por metodo post.-->
		<input type="submit" name="conveur" id="conveur" value="convertir euro">
	</form>

	<!--Formulario pesetas-euros-->
	<form action="controller/calcular.php" method="post">
		<fieldset>
			<legend>Convertidor Pesetas-Euros</legend>
			<label for="pes">Pesetas:</label>
			<input type="text" id="pes" name="pes"><br>
		</fieldset>
		<!--Submit para enviar los datos al php por metodo post.-->
		<input type="submit" name="convpes" id="convpes" value="convertir pesetas">
	</form>


<?php
	include "includes/footer.php";
?>
