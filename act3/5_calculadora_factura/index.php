<?php
	// Titulo de la pagina.
	$title = "Calculadora factura.";
	include "includes/header.php";

?>
	<!--Formulario, calculadora de facturas.-->
	<form action="controller/calcular.php" method="post">
		<fieldset>
			<legend>Facturas totales</legend>
			<label for="precio">Precio: </label>
			<input type="text" id="precio" name="precio"><br>
		</fieldset>
		<!--Submit para enviar los datos al php por metodo post.-->
		<input type="submit" name="calcular" id="calcular" value="calcular">
	</form>
<?php
	include "includes/footer.php";
?>
