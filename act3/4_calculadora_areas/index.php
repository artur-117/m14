<?php
	// Titulo de la pagina.
	$title = "Calculadora Geometrica";
	include "includes/header.php";
?>
	<!--Formulario, calculadora del area de un rectangulo.-->
	<form action="controller/calcular.php" method="post">
		<fieldset>
			<legend>Calculadora area rectangulo</legend>
			<label for="alt">Altura:</label>
			<input type="text" id="alt" name="alt"><br>
			<label for="base">Base:</label>
			<input type="text" id="base" name="base"><br>
		</fieldset>
		<!--Submit para enviar los datos al php por metodo post.-->
		<input type="submit" name="calcrt" id="calcrt" value="calcular rectangulo">
	</form>
	
	<!--Formulario, calculadora del area de un triangulo.-->
	<form action="controller/calcular.php" method="post">
		<fieldset>
			<legend>Calculadora area triangulo</legend>
			<label for="alt">Altura:</label>
			<input type="text" id="alt" name="alt"><br>
			<label for="base">Base:</label>
			<input type="text" id="base" name="base"><br>
		</fieldset>
		<!--Submit para enviar los datos al php por metodo post.-->
		<input type="submit" name="calct" id="calct" value="calcular triangulo">
	</form>


<?php
	include "includes/footer.php";
?>
