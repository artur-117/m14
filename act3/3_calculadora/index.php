<?php
	// Titulo de la pagina.
	$title = "Calculadora de 2 numeros.";
	include "includes/header.php";

?>
	<!--Formulario para pillar los 2 numeros a calcular.-->
	<form action="controller/calcular.php" method="post">
		<fieldset>
			<legend>Calculadora de 2 numeros</legend>
			<label for="num1">Numero 1:</label>
			<input type="text" id="num1" name="num1"><br>
			<label for="num2">Numero 2:</label>
			<input type="text" id="num2" name="num2"><br>
		</fieldset>
		<!--Submits para enviar los datos al php por metodo post, depende el submit seleccionado hara
		un calculo o otro.-->
		<input type="submit" name="sumar" id="sumar" value="+">
		<input type="submit" name="restar" id="restar" value="-">
		<input type="submit" name="multiplicar" id="multiplicar" value="X">
		<input type="submit" name="dividir" id="dividir" value="/">
		<input type="submit" name="todo" id="todo" value="*">
	</form>



<?php
	include "includes/footer.php";
?>
