<?php
	// Titulo de la pagina.
	$title = "Multiplicadora";
	include "includes/header.php";

?>
	<!--Formulario para pillar los dos numeros a multiplicar.-->
	<form action="controller/calcular.php" method="post">
		<fieldset>
			<legend>Multiplicadora</legend>
			<label for="a">Numero 1:</label>
			<input type="text" id="a" name="a"><br>
			<label for="b">Numero 2:</label>
			<input type="text" id="b" name="b">
		</fieldset>
		<!--Submit para enviar los datos al php por metodo post.-->
		<input type="submit" name="calcular" id="calcular" value="calcular">
	</form>
<?php
	include "includes/footer.php";
?>
